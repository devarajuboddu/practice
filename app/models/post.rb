class Post
  include Mongoid::Document
  include Mongoid::Paperclip
  field :title, type: String
  field :description, type: String
  has_mongoid_attached_file :avatar,
		:styles => {
			:original => ['1920x1680>', :jpg],
			:small    => ['135x250>',   :jpg],
			:medium   => ['250x250#',    :jpg],
			:large    => ['800x450#',   :jpg]
		}, :convert_options => { :all => '-background white -flatten +matte' }
	validates_attachment_content_type :avatar, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  has_many :comment
end
